# Docker container for nsp-indexer
## Usage
To start, 2 manual actions are needed to be made because they are environment specific:
1. copy prod.keys into the custom-files directory
2. edit `docker-compose.yml` and change the source directory for /data/games

Once that is done you are ready to build.
```
docker-compose up --build
```
Once the build is complete press ctrl-c once to gracefully stop the container

Now you can use the following to start the container.  Start is much quicker than build.
```
docker start nsp-indexer
```
When the container is up, you'll be able to access it through http://your.ip:8000

## Environmental Variables
Variables that are in config.defaults.php can be customized using environmental variables from docker but prepending NSP_ to the beginning (EX: NSP_extPort)

The list of variables are as follows:
- `gameDir` Absolute Files Path, no trailing slash
- `contentUrl` Files URL, no trailing slash
- `allowedExtensions` extensions for files that nsp-indexer is allowed to process
- `skipExtension` file extension used to mute alert for an update or dlc
- `enableNetInstall` Enable Net Install feature
- `enableRename` Enable Rename feature
- `switchIp` Switch IP address for Net Install
- `netInstallSrc` Set to override the source address for Net Install
- `showWarnings` Show configuration warnings on page load
- `keyFile` - Path to 'prod.keys', must be readable by the webserver/php, but KEEP IT SECURE
- `titleDbFolder` Path to titledb folder in nut installation
- `extPort` port that is forwarded to nginx REQUIRED

### Tips
* if you need a prod.keys file, you can generate one with lockpick_RCM. (gbatemp.net is your friend)
* to add to tinfoil:
```
Protocol: http
Host: your.ip.address
Port: 8000
Path: /index.php?tinfoil=true
title: nsp indexer
```
