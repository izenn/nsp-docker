#!/bin/bash
git config --global --add safe.directory '*'
git config --global init.defaultBranch main

# alpine not creating links properly
PHP=$(apk info|grep php[0-9]*$)
PHPVER=$(echo $PHP|sed 's/php//')

ln -s /usr/bin/php-config$PHPVER /usr/bin/php-config
ln -s /usr/bin/phpize$PHPVER /usr/bin/phpize

apk add gmp-dev zstd zstd-dev gcc musl-dev make $PHP-dev py3-beautifulsoup4 py3-colorama py3-crypto py3-curl py3-filelock py3-flask py3-google-api-python-client py3-pillow py3-pip py3-requests py3-tqdm py3-urllib3 py3-watchdog py3-zstandard python3 py3-oauthlib py3-requests-oauthlib

cp /custom-files/php-www2.conf /config/php/www2.conf
cp /custom-files/nginx-default.conf /config/nginx/site-confs/default.conf

if ! grep -q output_buffering /config/php/php-local.ini; then
 echo "output_buffering=Off" >> /config/php/php-local.ini
fi

if test ! -f /usr/lib/php$PHPVER/modules/yazstd.so; then
  mkdir /tmp/php-ext-yazstd; cd /tmp/php-ext-yazstd
  git init && git pull https://gitlab.com/izenn/php-ext-yazstd.git && phpize && ./configure --with-libzstd && make && make install &&
  if ! grep -q yazstd.so /config/php/php-local.ini; then
    cat yazstd.ini >> /config/php/php-local.ini
  fi
fi

cd /config/www
rm index.html
git init
git pull https://gitlab.com/izenn/nsp-indexer.git

if ! pip list|grep -q google-auth-oauthlib; then
  pip install --break-system-packages google-auth-oauthlib
fi 2>/dev/null

mkdir /config/nut
cd /config/nut
git init
git pull https://github.com/blawar/nut.git
cp /custom-files/prod.keys /config/nut/keys.txt
cp /custom-files/titledb /etc/periodic/daily/titledb
chmod +x /etc/periodic/daily/titledb

echo "<?php " > /config/www/config.php ; env|grep NSP|awk '{print "$"$1"\";"}'|sed 's/=/="/; s/NSP_//' >> /config/www/config.php

if test -f /config/www/config.php; then
  titleDbFolder=$(grep titleDbFolder /config/www/config.php|cut -d\" -f2)
fi

if test -z $TITLEDBDIR; then 
  titleDbFolder=$(grep titleDbFolder /config/www/config.default.php|cut -d\" -f2)
fi

FILE="${titleDbFolder}/titles.json"

if test -f $FILE; then
  find $FILE -mtime +0 -exec /etc/periodic/daily/titledb \;
else 
  /etc/periodic/daily/titledb
fi

chown -R abc:abc /config/www
cp /custom-files/prod.keys /config
chown abc:abc /config/prod.keys

if test -f /config/www/config.php; then
  gameDir=$(grep gameDir /config/www/config.php|cut -d\" -f2)
fi

if test -z $GAMESDIR; then
  gameDir=$(grep gameDir /config/www/config.default.php|cut -d\" -f2)
fi

chown -R abc:abc $gameDir